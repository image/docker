Ubuntu based docker builder image for gitlab ci with rocker installed

Current Version v0.2

| Application | Version    |
| ----------- | ---------- |
| Docker      | 17.10.0-ce |
| Rocker      | 1.3.1      |