FROM ubuntu
LABEL maintainer ="Jaspreet Singh <dev@jas.bio>"

ENV DOCKER_VERSION 17.10.0-ce
ENV ROCKER_VERSION 1.3.1

ENV DOCKER_HOST tcp://docker:2375
RUN apt update
RUN apt install -y curl
RUN curl -fsSLO https://download.docker.com/linux/static/test/x86_64/docker-${DOCKER_VERSION}.tgz && tar --strip-components=1 -xvzf docker-${DOCKER_VERSION}.tgz -C /usr/local/bin
RUN curl -SL https://github.com/grammarly/rocker/releases/download/${ROCKER_VERSION}/rocker-${ROCKER_VERSION}-linux_amd64.tar.gz | tar -xzC /usr/local/bin && chown root:root /usr/local/bin/rocker && chmod +x /usr/local/bin/rocker